
all: lint test_verilator

lint: 
	$(MAKE) -C ./src clean
	$(MAKE) -C ./src lint
	
test_verilator:
	$(MAKE) -C ./test/verilator test_all
	
run_modelsim:
	@echo "NOT IMPLEMENTED"

snap:
	$(MAKE) -C ./src snap
