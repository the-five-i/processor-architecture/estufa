import config_pkg::*;

 module pageWalker
 (
  input logic clk,
  input logic res,

  input [XLEN-1:0] satp,

  pwBus itlb,
  pwBus dtlb,

  roBus bus
 );

  typedef enum { IDLE, VPN0, VPN1, VPN2, ACK } state_t;
  typedef enum { INST, DATA } source_t;

  state_t  state, nxState;
  source_t src,   nxSrc;

  logic [XLEN-1:0] pwAddr;
  logic pwLdp;

  logic [MLEN-1:0] base_addr, nx_base_addr;

  assign itlb.data = {itlb.addr[4] ? bus.ldData >> 64 : bus.ldData}[63:0];
  assign dtlb.data = {dtlb.addr[4] ? bus.ldData >> 64 : bus.ldData}[63:0];

  always_comb
  begin
    case(src)
      INST: pwAddr = itlb.addr;
      DATA: pwAddr = dtlb.addr;
    endcase

    case(src)
      INST: pwLdp = itlb.ptp;
      DATA: pwLdp = dtlb.ptp;
    endcase

    itlb.ptr <= 0;
    dtlb.ptr <= 0;
    case(src)
      INST: itlb.ptr <= state == ACK;
      DATA: dtlb.ptr <= state == ACK;
    endcase

    itlb.ptf <= 0;
    dtlb.ptf <= 0;
    case(src)
      INST: itlb.ptf = state == ACK ? ~itlb.data[0] : 0;
      DATA: dtlb.ptf = state == ACK ? ~dtlb.data[0] : 0;
    endcase
  end

  logic [XLEN-1:0] shifted_addr;

  always_comb
  begin
    case(state)
      VPN2:    shifted_addr = pwAddr >> 12;
      VPN1:    shifted_addr = pwAddr >> 21;
      default: shifted_addr = pwAddr >> 30;
    endcase
  end

  always_comb
  begin
    case(state)
      VPN2:    bus.addr = {{MBLEN-VPNLEN{1'b0}},shifted_addr[VPNLEN-1:0]} + base_addr;
      VPN1:    bus.addr = {{MBLEN-VPNLEN{1'b0}},shifted_addr[VPNLEN-1:0]} + base_addr;
      default: bus.addr = {{MBLEN-VPNLEN{1'b0}},shifted_addr[VPNLEN-1:0]} + satp;
    endcase
    case(state)
      VPN2:    bus.ldp = pwLdp;
      VPN1:    bus.ldp = pwLdp;
      VPN0:    bus.ldp = pwLdp;
      ACK:     bus.ldp = 0;
      default: bus.ldp = 0;
    endcase
  end

  always_comb
  begin

    nx_base_addr <= base_addr;
    nxState      <= state;
    nxSrc        <= src;

    case(state)
      IDLE: begin
        // TODO: check if ptp @ fetch stage is working.
        // TODO: check if ptp when enabled forwards the petition upwards.
        if ( itlb.ptp ) begin
          nxState <= VPN0;
          nxSrc   <= INST;
        end else if ( dtlb.ptp ) begin
          nxState <= VPN0;
          nxSrc   <= DATA;
        end
      end

      VPN0: begin
        case(src)
          INST: if ( bus.ldr ) begin
            nxState <= VPN1;
            nx_base_addr <= {bus.ldData[15:8], 12'b0};
            if (~itlb.data[0]) nxState <= ACK;
          end

          DATA: if ( bus.ldr ) begin
            nxState <= VPN1;
            nx_base_addr <= {bus.ldData[15:8], 12'b0};
            if (~dtlb.data[0]) nxState <= ACK;
          end
        endcase
      end

      VPN1: begin
        case(src)
          INST: if ( bus.ldr ) begin
            nxState <= VPN2;
            nx_base_addr <= {bus.ldData[15:8], 12'b0};
            if (~itlb.data[0]) nxState <= ACK;
          end

          DATA: if ( bus.ldr ) begin
            nxState <= VPN2;
            nx_base_addr <= {bus.ldData[15:8], 12'b0};
            if (~dtlb.data[0]) nxState <= ACK;
          end
        endcase
      end

      VPN2: begin
        case(src)
          INST: if ( bus.ldr ) nxState <= ACK;
          DATA: if ( bus.ldr ) nxState <= ACK;
        endcase
      end

      // ACK (wait one cicle for tlb to grab data)
      default: nxState <= IDLE;

    endcase

  end

  always_ff @(posedge clk)
  begin
    state = nxState;
    src   = nxSrc;

    base_addr = nx_base_addr;
  end
 
 endmodule
