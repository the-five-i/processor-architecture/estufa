import config_pkg::*;
import instruction_pkg::*;

module MStage (
  input  decodedInstruction_t instruction_i,
  output decodedInstruction_t instruction_o,
);

	logic [XLEN-1:0] rs1 = instruction_i.src1;
	logic [XLEN-1:0] rs2 = instruction_i.src2;

	logic unsigned [XLEN-1:0] rs1u; assign rs1u = rs1;
	logic unsigned [XLEN-1:0] rs2u; assign rs2u = rs2;

	logic signed [XLEN-1:0] rs1s; assign rs1s = rs1;
	logic signed [XLEN-1:0] rs2s; assign rs2s = rs2;

	logic [2*XLEN-1:0] rMul;
	logic [2*XLEN-1:0] rMulh;
	logic [2*XLEN-1:0] rMulhsu;
	logic [2*XLEN-1:0] rMulhu;
	logic [XLEN-1:0] rDiv;
	logic [XLEN-1:0] rDivu;
	logic [XLEN-1:0] rRem;
	logic [XLEN-1:0] rRemu;

	logic [XLEN-1:0] result;

	assign rMul=rs1u*rs2u;
	assign rMulh=rs1s*rs2s;
	assign rMulhsu=rs1s*rs2u;
	assign rMulhu=rs1u*rs2u;
	assign rDiv=rs1s/rs2s;
	assign rDivu=rs1u/rs2u;
	assign rRem=rs1s/rs2s;
	assign rRemu=rs1u/rs2u;

	

	always_comb begin
		case(func3)
	  //0 MUL
	  0:
			assign result=rMul[XLEN-1:0];
    //1 MULH
	  1:
			assign result=rMulh[2*XLEN-1:XLEN];
    //2 MULHSU
		2:
			assign result=rMulshu[2*XLEN-1:XLEN];
    //3 MULHU
    3:
			assign result=rMulhu[2*XLEN-1:XLEN];
    //4 DIV
    4:
			assign result=rDiv;
    //5 DIVU
    5:
			assign result=rDivu;
    //6 REM
    6:
			assign result=rRem;
    //7 REMU
    7:
			assign result=rRemu;
		endcase
		assign instruction_o=instruction_i;
		assign instruction_o.ddst=result;
		assign instruction_o.rdst=1;
	end
	
endmodule
