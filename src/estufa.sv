
module estufa ( input logic clk, input logic res );

  rwBus mmBUS();

  mainMemory mm (
    .clk,
    .res,
    .bus(mmBUS)
  );

  rwBus msBUS();

  memoryBuffer mb (
    .clk,
    .res,

    .bus_o(mmBUS),
    .bus_i(msBUS)
  );

  roBus instBUS();
  rwBus dataBUS();
  roBus pageBUS();

  memorySwitch ms (
    .clk,
    .res,

    .mainBUS(msBUS),

    .instBUS(instBUS),
    .dataBUS(dataBUS),
    .pageBUS(pageBUS)
  );

  pipeline pipeline_0 (
    .clk,
    .res,

    .instBUS(instBUS),
    .dataBUS(dataBUS),
    .pageBUS(pageBUS)
  );

endmodule


