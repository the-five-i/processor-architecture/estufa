import config_pkg::*;
import instruction_pkg::*;

module memorySwitch
(
  input  logic clk,
  input  logic res,

  rwBus.ll mainBUS,

  roBus.hl instBUS,
  rwBus.hl dataBUS,
  roBus.hl pageBUS
);

  typedef enum { IDLE, BUSSY } state_t;
  typedef enum { INST, DATA, PAGE } source_t;

  state_t   state, nxState;
  source_t  src, nxSrc;

  logic [MLEN-1:0] mainAddr;
  logic mainLdp, mainStp;

  // Main bus
  assign mainBUS.addr = mainAddr;
  assign mainBUS.ldp  = state == BUSSY ? mainLdp : 0;
  assign mainBUS.stp  = state == BUSSY ? mainStp : 0;
  assign mainBUS.stData = dataBUS.stData;

  // Page bus
  assign pageBUS.ldr = src == PAGE ? mainBUS.ldr : 0;
  assign pageBUS.ldData = mainBUS.ldData;

  // Inst bus
  assign instBUS.ldr = src == INST ? mainBUS.ldr : 0;
  assign instBUS.ldData = mainBUS.ldData;

  // Data bus
  assign dataBUS.ldr = src == DATA ? mainBUS.ldr : 0 ;
  assign dataBUS.str = src == DATA ? mainBUS.str : 0 ;
  assign dataBUS.ldData = mainBUS.ldData;


  // Main Addr
  always_comb
  begin
    case(src)
      INST: mainAddr = instBUS.addr;
      DATA: mainAddr = dataBUS.addr;
      PAGE: mainAddr = pageBUS.addr;
      default: mainAddr = 0;
    endcase
  end

  // Main ldp
  always_comb
  begin
    case(src)
      INST: mainLdp = instBUS.ldp;
      DATA: mainLdp = dataBUS.ldp;
      PAGE: mainLdp = pageBUS.ldp;
      default: mainLdp = 0;
    endcase
  end

  // Main stp
  always_comb
  begin
    case(src)
      INST: mainStp = 0;
      DATA: mainStp = dataBUS.stp;
      PAGE: mainStp = 0;
      default: mainStp = 0;
    endcase
  end

  // State & Source Control
  always_ff @(posedge clk)
    state = nxState;

  always_ff @(posedge clk)
    src = nxSrc;

  always_comb
  begin
    case(state)
      IDLE: begin
        if ( pageBUS.ldp ) begin
          nxState <= BUSSY;
          nxSrc   <= PAGE;
        end else if ( instBUS.ldp ) begin
          nxState <= BUSSY;
          nxSrc   <= INST;
        end else if ( dataBUS.ldp | dataBUS.stp ) begin
          nxState <= BUSSY;
          nxSrc   <= DATA;
        end
      end

      BUSSY: begin
        case(src)
          PAGE: if ( pageBUS.ldr ) nxState <= IDLE;
          INST: if ( instBUS.ldr ) nxState <= IDLE;
          DATA: if ( dataBUS.ldr ) nxState <= IDLE;
          default: begin
            nxState <= IDLE;
            nxSrc   <= PAGE;
          end
        endcase
      end

      default: nxState <= IDLE;

    endcase
  end

endmodule
