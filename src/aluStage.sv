
import config_pkg::*;
import branch_pkg::*;
import instruction_pkg::*;

module aluStage (
  input  decodedInstruction_t instruction_i,
  output decodedInstruction_t instruction_o,
  output evaluatedBranch_t    evaluatedBranch
);

  // Parsing instruction in
  logic isRrw; assign isRrw = instruction_i.isRrw;
  logic isRr; assign isRr = instruction_i.isRr;
  logic isIm; assign isIm = instruction_i.isIm;
  logic isLd; assign isLd = instruction_i.isLd;
  logic isSt; assign isSt = instruction_i.isSt;
  logic isBr; assign isBr = instruction_i.isBr;

  logic [6:0] func7; assign func7 = instruction_i.func7;
  logic [6:0] func3; assign func3 = instruction_i.func3;

  logic [XLEN-1:0] A; assign A = instruction_i.src1;
  logic [XLEN-1:0] B; assign B = instruction_i.src2;
  logic [XLEN-1:0] C; assign C = instruction_i.src3;

  branchPrediction_t bp; assign bp = instruction_i.bp;
  //
  logic [XLEN-1:0] W;
  //
  // Parsing instruction out
  always_comb
  begin
    instruction_o = instruction_i;

    if (isRrw | isRr | isIm | isLd | isSt) 
      instruction_o.ddst = W;
    instruction_o.rdst = instruction_i.rdst | isRr | isRrw | isIm;
  end

	logic   signed [XLEN-1:0] sA;
	logic   signed [XLEN-1:0] sB;
	logic unsigned [XLEN-1:0] uA;
	logic unsigned [XLEN-1:0] uB;

	assign sA =   signed'(A);
	assign uA = unsigned'(A);
	assign sB =   signed'(B);
	assign uB = unsigned'(B);

	logic [XLEN-1:0] signedSum;
	assign signedSum = sA + sB; 

	logic [XLEN-1:0]  pcBR;
	logic takeBR;

	logic brEQ;
	logic brNE;
	logic brLT;
	logic brGE;
	logic brLTU;
	logic brGEU;

	assign brEQ  = sA == sB;
	assign brNE  = ! brEQ;
	assign brLT  = sA < sB;
	assign brGE  = ! brLT;
	assign brLTU = uA < uB;
	assign brGEU = ! brLTU;

	always_comb
	begin
		case(func3)
			default: takeBR = brEQ;
			3'b001:  takeBR = brNE;
			3'b100:  takeBR = brLT;
			3'b101:  takeBR = brGE;
			3'b110:  takeBR = brLTU;
			3'b111:  takeBR = brGEU;
		endcase

	end

	assign pcBR = C + bp.pc;

  always_comb
  begin
    if (instruction_i.valid) begin
      if (~isBr) begin
        evaluatedBranch.miss = bp.valid;
      end else begin
        if (bp.valid) 
          evaluatedBranch.miss = (bp.taken ^ takeBR) | (bp.taken & takeBR & ~(bp.takenPC == pcBR));
        else
          evaluatedBranch.miss = takeBR;
      end
    end else begin
      evaluatedBranch.miss = 0;
    end
  end

  assign evaluatedBranch.taken = takeBR & isBr;
  assign evaluatedBranch.pc = bp.pc;
  assign evaluatedBranch.takenPC = pcBR;
  assign evaluatedBranch.implicitPC = bp.implicitPC;

  logic [XLEN-1:0] riAdd;
  logic [XLEN-1:0] riSetLess;
  logic [XLEN-1:0] riSetLessU;
  logic [XLEN-1:0] riXor;
  logic [XLEN-1:0] riOr;
  logic [XLEN-1:0] riAnd;
  logic [XLEN-1:0] riSlli;
  logic [XLEN-1:0] riSr;

  assign riAdd       = signedSum;
  assign riSetLess   = { {XLEN{1'b0}}, sA < sB };
  assign riSetLessU  = { {XLEN{1'b0}}, uA < uB };
  assign riXor       = uA ^ uB;
  assign riOr        = uA | uB;
  assign riAnd       = uA & uB;

  assign riSlli      = sA << sB[5:0];
  assign riSr        = sB[31:26] == 16 ? uA >>> sB[5:0] : uA >> sB[5:0];

	logic [XLEN-1:0] ri;
	always_comb
	begin
		case(func3)
			default: ri = riAdd;
      3'b001:  ri = riSlli;
			3'b010:  ri = riSetLess;
			3'b011:  ri = riSetLessU;
			3'b100:  ri = riXor;
      3'b101:  ri = riSr;
			3'b110:  ri = riOr;
			3'b111:  ri = riAnd;
		endcase
	end

	logic [XLEN-1:0] rr;
  logic [XLEN-1:0] rrAdd;
  logic [XLEN-1:0] rrShiftLeft;
  logic [XLEN-1:0] rrSetLess;
  logic [XLEN-1:0] rrSetLessU;
  logic [XLEN-1:0] rrXOR;
  logic [XLEN-1:0] rrShiftRight;
  logic [XLEN-1:0] rrOR;
  logic [XLEN-1:0] rrAND;

  assign rrAdd        = func7[5] ? sA - sB : signedSum;
  assign rrShiftLeft  = sA << sB[5:0];
  assign rrSetLess    = { {XLEN-1{1'b0}}, sA < sB };
  assign rrSetLessU   = { {XLEN-1{1'b0}}, uA < uB };
  assign rrXOR        = uA ^ uB;
  assign rrShiftRight = func7[5] ? uA >>> sB[5:0] : uA >> sB[5:0];
  assign rrOR         = uA | uB;
  assign rrAND        = uA & uB;

	always_comb
	begin
		case(func3)
			default: rr = rrAdd;
			3'b001:  rr = rrShiftLeft;
			3'b010:  rr = rrSetLess;
			3'b011:  rr = rrSetLessU;
			3'b100:  rr = rrXOR;
			3'b101:  rr = rrShiftRight;
			3'b110:  rr = rrOR;
			3'b111:  rr = rrAND;
		endcase
	end

	logic [XLEN-1:0] rrw;
  logic [XLEN-1:0] rrwAdd;
  logic [XLEN-1:0] rrwShiftLeft;
  logic [XLEN-1:0] rrwShiftRight;

  logic [XLEN-1:0] signedSub;
  assign signedSub = sA - sB;

  assign rrwAdd        = func7[5] ? {{32{signedSub[31]}}, signedSub[31:0]} :
                                    {{32{signedSum[31]}}, signedSum[31:0]};

  assign rrwShiftLeft  = sA << sB[4:0];
  assign rrwShiftRight = func7[5] ? uA >>> sB[4:0] : uA >> sB[4:0];

	always_comb
	begin
		case(func3)
			default: rrw = rrwAdd;
			3'b001:  rrw = rrwShiftLeft;
			3'b101:  rrw = rrwShiftRight;
		endcase
	end

	logic [XLEN-1:0]  ld,  st;
	assign ld = signedSum;
	assign st = signedSum;

	always_comb
	begin
    if (isLd) W = ld; else 
    if (isSt) W = st; else
    if (isIm) W = ri; else 
    if (isRr) W = rr; else
    if (isRrw) W = rrw; else
              W = 0;
	end

endmodule
