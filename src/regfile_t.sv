
import config_pkg::*;


module regfile_t
#(
  parameter integer addrSize = $clog2(RFLEN),
  parameter integer numberOfRegisters = RFLEN
)
(
  input logic clk,
  input logic res,

  input  logic [addrSize-1:0] sr1Addr,
  output logic         [XLEN-1:0] sr1Data,

  input  logic [addrSize-1:0] sr2Addr,
  output logic         [XLEN-1:0] sr2Data,

  input  logic                drWrEn,
  input  logic [addrSize-1:0] drAddr,
  input  logic         [XLEN-1:0] drData
);

  logic  nclk;
  assign nclk = ~clk;
  logic [XLEN-1:0]    registers [numberOfRegisters-1:0];
  logic [XLEN-1:0] nxregegister [numberOfRegisters-1:0];

  assign sr1Data = registers[sr1Addr];
  assign sr2Data = registers[sr2Addr];

  genvar i;
  generate 

    // Register 0
    always_comb
      nxregegister[0] = 0;
    always_ff @(posedge nclk)
      registers[0] = nxregegister[0];

    for(i=1; i < numberOfRegisters; i++) 
    begin : regfile_control
      always_comb
      begin
        if (res) 
          nxregegister[i] = {XLEN-1{1'd0}};
        else if (i == drAddr && drWrEn)
            nxregegister[i] = drData;
        else
          nxregegister[i] = registers[i];
      end

      always_ff @(posedge nclk)
        registers[i] = nxregegister[i];

    end
  endgenerate

endmodule
