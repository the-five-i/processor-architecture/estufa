
package config_pkg;

  parameter integer XLEN = 64;  // Architecture lenght
  parameter integer ILEN = 32;  // Instruction lenght

  parameter integer MLEN = 20;  // Physical memory address
  
  parameter integer MBLEN = 128; // Memory bus length in bits 

  parameter integer DCLEN  = 2;  // Number of lines in the data cache
  parameter integer DCLLEN = MBLEN; // Data cache line size in bits

  parameter integer TLBLEN = 2;  // TLB size in lines
  parameter integer VPNLEN = 28;
  parameter integer PPNLEN = 16;

  parameter integer RFLEN = 32; // Register File size

  parameter integer BPLEN = 4; // Branch Predictor size

  // TODO: add here the cache paramatrization
  /*
  parameter integer ICLEN  = 4;     // Number of lines in the instruction cache
  parameter integer ICLLEN = MBLEN; // Instruction cache line size in ILENs
  */

  //
  parameter integer GLLEN = 64; // Maximum of 64 registers to be written so..
  //
  // addr 0 exception return PC
  // addr 1 satp register
  // addr 2 exception type
  // addr 3 user mode
  parameter integer CSRLEN = 5;

endpackage
