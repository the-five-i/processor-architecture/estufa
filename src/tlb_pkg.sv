
package tlb_pkg;

  typedef struct {
    logic D; // Dirty     (ignored)
    logic A; // Accessed  (ignored)
    logic G; // Global    (ignored)
    logic U; // User
    logic X; // Execution
    logic W; // Write
    logic R; // Read
    logic V; // Valid
  } tlb_flags_t;
  
 endpackage
