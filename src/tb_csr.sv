`timescale 1ns / 1ps

import config_pkg::*;

module tb_csr
#(
  parameter CORE_CLOCK_PERIOD = 4
)
() ;

  logic clk;
  logic res = 1;

  always #(CORE_CLOCK_PERIOD/2)
    clk = ~clk;

  initial res = 1;
  initial clk = 1;

  integer i = 0;

  logic wrEn;
  logic [11:0]  wrAddr;
  logic [XLEN-1:0] wrData;

  logic [CSRLEN-1:0][XLEN-1:0] csrData;


  always @(posedge clk) begin
    if (!i)
      i <= i+1;
    else
      res <= 0;
  end

  initial #10
  begin
    wrEn   = 1;
    wrAddr = 4;
    wrData = 0;
  end

  initial #(10+CORE_CLOCK_PERIOD)
  begin
    wrEn   = 0;
    wrAddr = 0;
    wrData = 0;
  end

  initial #20
  begin
    wrEn   = 1;
    wrAddr = 0;
    wrData = 64'h0001_000;
  end

  initial #(20+CORE_CLOCK_PERIOD)
  begin
    wrEn   = 0;
    wrAddr = 0;
    wrData = 0;
  end

  controlStateRegisterFile csr_0 (
    .clk,
    .res,

    .csrData,

    .wrEn,
    .wrAddr,
    .wrData
  );

endmodule
