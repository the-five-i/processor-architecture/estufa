# Estufa Processor

Because it warms your HART.

## Description

This project started as a project for the PA-MIRI assignment at FIB. It's objective is to implement the RISC-V RV64IMA 
extencions. And maybe in the future include the RV64F. 

## Implemented instructions

Based on the instructions at the [RISC-V ISA Manual](https://github.com/riscv/riscv-isa-manual) and [RISC-V Operation Codes Repository](https://github.com/riscv/riscv-opcodes) we have implemented the following instructions:

## License

This project is under the GNU Affero General Public License, check the LICENCE file for more info.
