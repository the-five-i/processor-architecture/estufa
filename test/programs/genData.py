import sys

def main():
    size=sys.argv[1]
    pattern=sys.argv[2]

    x = lambda a : 0
     
    if pattern == 'one':
        x = lambda a : 1
    elif pattern == 'onezero':
        x = lambda a : a%2
    elif pattern == 'inc':
        x = lambda a : a
    elif pattern == 'ptwo':
        x = lambda a : 2**a
    
    for i in range(int(size)):
        if i%4 == 0:
            st=""
            
        st='{0:08x}'.format(x(i)) + st

        if i%4 == 3:
            print(st) 

if __name__ == '__main__':
    main()
