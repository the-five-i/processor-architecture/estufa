
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "VpcStage.h"
#include "verilated.h"

int main(int argc, char **argv) {

  Verilated::commandArgs(argc, argv);

  VpcStage *tb = new VpcStage;

  tb -> res = 1;
  tb -> pcFetch = 0x5555;

  tb -> pcALU     = 0xAFAF;
  tb -> injectALU = 0;

  tb -> iblock = 0;

  tb -> eval();

  assert(tb -> nxpc == 0x0100);

  tb -> res = 0;
  tb -> pcFetch = 0x5555;

  tb -> pcALU     = 0xAFAF;
  tb -> injectALU = 0;

  tb -> iblock = 0;

  tb -> eval();

  assert(tb -> nxpc == 0x5559);

  tb -> res = 0;
  tb -> pcFetch = 0x5555;

  tb -> pcALU     = 0xAFAF;
  tb -> injectALU = 1;

  tb -> iblock = 0;

  tb -> eval();

  assert(tb -> nxpc == 0xAFAF);

  tb -> res = 0;
  tb -> pcFetch = 0x5555;

  tb -> pcALU     = 0xAFAF;
  tb -> injectALU = 0;

  tb -> iblock = 1;

  tb -> eval();

  assert(tb -> nxpc == 0x5555);

  tb -> res = 0;
  tb -> pcFetch = 0x5555;

  tb -> pcALU     = 0xAFAF;
  tb -> injectALU = 1;

  tb -> iblock = 1;

  tb -> eval();

  assert(tb -> nxpc == 0x5555);

  tb -> res = 1;
  tb -> pcFetch = 0x5555;

  tb -> pcALU     = 0xAFAF;
  tb -> injectALU = 1;

  tb -> iblock = 1;

  tb -> eval();

  assert(tb -> nxpc == 0x0100);

}
